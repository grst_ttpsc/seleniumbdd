package utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Helper {
    private static Helper Helper;
    private static WebDriver driver ;

    private Helper() {
        ChromeOptions chromeOptions = new ChromeOptions();
        System.setProperty("webdriver.chrome.silentOutput", "true");
        driver = new ChromeDriver();
    }
    public static WebDriver getDriver() {
        return driver;
    }
    public static void setUpDriver() {
        if (Helper==null) {
            Helper = new Helper();
        }
    }
    public static void tearDown() {
        if(driver!=null) {
            driver.close();
            driver.quit();
        }
        Helper = null;
    }
}
