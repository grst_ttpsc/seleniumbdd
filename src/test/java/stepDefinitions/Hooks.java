package stepDefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import utilities.Helper;

public class Hooks {
    @Before
    public static void setUp() {
        Helper.setUpDriver();
    }

    @After
    public static void tearDown(Scenario scenario) {
        Helper.tearDown();
    }
}
