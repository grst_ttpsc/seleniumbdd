package stepDefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pl.ttpsc.selenium.pages.InventoryPage;
import pl.ttpsc.selenium.pages.LoginPage;
import utilities.Helper;

public class InventorySteps {
    public InventoryPage inventoryPage;

    public InventorySteps() {
        inventoryPage = new InventoryPage(Helper.getDriver());
    }
    @Then("I see inventory page")
    public void i_see_inventory_page() {
        inventoryPage.checkIfNavbarIsDisplayed();
    }

    @When("I logout user")
    public void i_click_logout_button() {
        inventoryPage.logout();
    }
}
