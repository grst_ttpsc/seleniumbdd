package stepDefinitions;

import io.cucumber.java.en.Given;
import pl.ttpsc.selenium.pages.LoginPage;
import utilities.Helper;

public class BaseClass {

    @Given("I am on login page")
    public void getWebsite(){
        Helper.getDriver().get("https://www.saucedemo.com/");
    }
}
