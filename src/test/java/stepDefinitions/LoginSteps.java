package stepDefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pl.ttpsc.selenium.pages.LoginPage;
import utilities.Helper;

public class LoginSteps {
    public LoginPage loginPage;

    public LoginSteps() {
        loginPage = new LoginPage(Helper.getDriver());
    }
    @When("I fill login form with {} and {}")
    public void i_fill_login_form_with(String username, String password) {
        loginPage.login(username, password);
    }

    @Then("I see login page")
    public void i_see_login_page() {
        loginPage.checkIfLoginFormIsDisplayed();
    }
}
