Feature: Login form tests
  Scenario: Login as Standard user
    Given I am on login page
    When I fill login form with standard_user and secret_sauce
    Then I see inventory page
  Scenario: Logout standard user
    Given I am on login page
    And I fill login form with standard_user and secret_sauce
    When I logout user
    Then I see login page
  Scenario: Login as Locked out user
    Given I am on login page
    When I fill login form with locked_out_user and secret_sauce
    Then I see inventory page