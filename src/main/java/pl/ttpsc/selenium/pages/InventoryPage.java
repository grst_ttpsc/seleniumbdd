package pl.ttpsc.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class InventoryPage extends BasePage {
    @FindBy(className = "app_logo")
    private WebElement lblNavbarHeader;
    @FindBy(className = "bm-burger-button")
    private WebElement btnBurger;
    @FindBy(id = "logout_sidebar_link")
    private WebElement btnLogout;
    @FindBy(id = "add-to-cart-sauce-labs-backpack")
    private WebElement btnAddSauceLabsBackpack;
    @FindBy(id = "shopping_cart_container")
    private WebElement btnCart;

    public InventoryPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void logout() {
        btnBurger.click();
        btnLogout.click();
    }

    public void checkIfNavbarIsDisplayed() {
        assertTrue(lblNavbarHeader.isDisplayed());
    }

    public void addToCartSauceLabsBackpack() {
        btnAddSauceLabsBackpack.click();
    }

    public void goToCart() {
        btnCart.click();
    }
}
